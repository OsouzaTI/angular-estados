import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InteiroComponent } from './components/inteiro/inteiro.component';
import { TuiInputNumberModule } from '@taiga-ui/kit';
import { LabelComponent } from './components/label/label.component';
import { TuiLabelModule } from '@taiga-ui/core';



@NgModule({
  declarations: [
    InteiroComponent,
    LabelComponent
  ],
  imports: [
    CommonModule,
    TuiInputNumberModule,
    TuiLabelModule
  ],
  exports: [
    InteiroComponent,
    LabelComponent
  ]
})
export class CampoModule { }
