import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-inteiro',
  templateUrl: './inteiro.component.html',
  styleUrls: ['./inteiro.component.css']
})
export class InteiroComponent {

  @Input()
  public campo : number = 0;

}
