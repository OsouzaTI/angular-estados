import { BehaviorSubject, Observable } from "rxjs";

export class Store<T> {

    private stateObservable: Observable<T>;
    private stateBehaviorSubject : BehaviorSubject<T>;

    constructor(private initialState: T) {
        this.stateBehaviorSubject = new BehaviorSubject(initialState);
        this.stateObservable = this.stateBehaviorSubject.asObservable();
    }

    get state(): T {
        return this.stateBehaviorSubject.getValue();
    }

    setState(value : T) : void {
        this.stateBehaviorSubject.next(value);
    }

}